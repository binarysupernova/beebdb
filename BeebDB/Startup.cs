﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BeebDB.Startup))]
namespace BeebDB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
